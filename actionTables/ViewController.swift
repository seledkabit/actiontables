//
//  ViewController.swift
//  actionTables
//
//  Created by User on 21.11.2020.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let storyboard = UIStoryboard(name: "MainScreen", bundle: nil)
        if let mainScreen = storyboard.instantiateViewController(identifier: "MainVC") as? StartScreenViewController {
            mainScreen.modalPresentationStyle = .fullScreen
            
            present(mainScreen, animated: true) {
                
            }
        }
    }
}

