//
//  Question.swift
//  actionTables
//
//  Created by User on 21.11.2020.
//

import Foundation

struct Question {
    let questionStr: String
    let answers: [String]
    var correctAnswerIndex: Int
    
}

typealias Questions = [Question]
