//
//  QuestionViewController.swift
//  actionTables
//
//  Created by User on 21.11.2020.
//

import UIKit

class QuestionViewController: UIViewController {

    @IBOutlet weak var answerFirst: UIButton!
    @IBOutlet weak var answerSecond: UIButton!
    @IBOutlet weak var answerThird: UIButton!
    @IBOutlet weak var answerFourth: UIButton!
    
    private var correctAnswer: Int = 0
    private var questionData: Question?
    
    @IBOutlet weak var question: UILabel!
    
    @IBOutlet weak var answerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    func setQuestion(quest: Question) {
        questionData = quest
    }
    
    func setupView() {
        guard let quest = questionData else {
            return
        }
        
        question.text = quest.questionStr
        
        answerFirst.setTitle(quest.answers[0], for: .normal)
        answerSecond.setTitle(quest.answers[1], for: .normal)
        answerThird.setTitle(quest.answers[2], for: .normal)
        answerFourth.setTitle(quest.answers[3], for: .normal)
        correctAnswer = quest.correctAnswerIndex;
    }
    
    func correctAnswerCheck(index: Int) {
        if index == correctAnswer {
            answerView.backgroundColor = #colorLiteral(red: 0, green: 0.9768045545, blue: 0, alpha: 0.8)
        } else {
            answerView.backgroundColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 0.8)
        }
        answerView.isHidden = false
    }
    
    @IBAction func onTouchFirstAnswer(_ sender: Any) {
        correctAnswerCheck(index: 0)
    }
    @IBAction func onTouchSecondAnswer(_ sender: Any) {
        correctAnswerCheck(index: 1)
    }
    @IBAction func onTouchThirdAnswer(_ sender: Any) {
        correctAnswerCheck(index: 2)
    }
    @IBAction func onTouchFourthAnswer(_ sender: Any) {
        correctAnswerCheck(index: 3)
    }
    @IBAction func onTouchNextButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Quizz", bundle: nil)
        if let quizzVC = storyboard.instantiateViewController(identifier: "QuizzVC") as? QuestionViewController {
            quizzVC.modalPresentationStyle = .fullScreen
            guard let question = QuestionService.shaped.getNextQuestion() else {
                return
            }
            
            quizzVC.setQuestion(quest: question)
            present(quizzVC, animated: true) {
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
