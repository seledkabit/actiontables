//
//  StartScreenViewController.swift
//  actionTables
//
//  Created by User on 21.11.2020.
//

import UIKit

class StartScreenViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        QuestionService.shaped.createQuestionQueue()
    }
    
    @IBAction func StartButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Quizz", bundle: nil)
        if let quizzVC = storyboard.instantiateViewController(identifier: "QuizzVC") as? QuestionViewController {
            quizzVC.modalPresentationStyle = .fullScreen
            guard let question = QuestionService.shaped.getNextQuestion() else {
                return
            }
            quizzVC.setQuestion(quest: question)
            
            present(quizzVC, animated: true) {
                
            }
        }
    }
    
    

}
