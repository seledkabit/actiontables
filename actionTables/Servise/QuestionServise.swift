//
//  QuestionServise.swift
//  actionTables
//
//  Created by User on 21.11.2020.
//

import Foundation

class QuestionService {
    static let shaped = QuestionService()
    private var queueQuestions: Questions = Questions()
    private var currentQuestionIndex: Int = -1
    private var numberOfCorrectAnswers: Int = 0
    
    
    func createQuestionQueue() {
        currentQuestionIndex = -1
        numberOfCorrectAnswers = 0
        
        queueQuestions.append(
            Question(
                questionStr: "how much?",
                answers: ["1", "2", "3", "4"],
                correctAnswerIndex: 1
            )
        )
        
        queueQuestions.append(
            Question(
                questionStr: "12334",
                answers: ["fgh", "ghfch", "vnv", "bnvbvbg"],
                correctAnswerIndex: 2
            )
        )
        
//        queueQuestions.append(
//            Question(
//                questionStr: <#String#>,
//                answers: <#T##[String]#>,
//                correctAnswerIndex: <#T##Int#>
//            )
//        )
//
//        queueQuestions.append(
//            Question(
//                questionStr: <#String#>,
//                answers: <#T##[String]#>,
//                correctAnswerIndex: <#T##Int#>
//            )
//        )
    }
    
    func getNextQuestion() -> Question? {
        currentQuestionIndex += 1
        if currentQuestionIndex >= queueQuestions.count{
            return nil
        } else {
            return queueQuestions[currentQuestionIndex]
        }
    }
}
